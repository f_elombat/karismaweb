<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Karisma :: @yield('title', "Concepts")</title>
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- Core Style Sheets -->

    {{-- <link rel="stylesheet" href="{{ asset('css/all.css') }}"> --}}


    <!-- Core Style Sheets -->
    <link rel="stylesheet" href="{{ asset('assets/css/master.css') }}">
    <!-- Responsive Style Sheets -->
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <!-- Revolution Style Sheets -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/settings.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/layers.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/navigation.css') }}">

    @php
        //use Illuminate\Support\Facades\URL;

        $menu_list = [
            [
                'title' => 'Concepts',
                'hash' => 'home',
                'url' => asset('/home/concepts'),
            ],
            [
                'title' => 'Services',
                'hash' => 'feature',
                'url' => asset('/services'),
            ],
            [
                'title' => 'Projects',
                'hash' => 'portfolio',
                'url' => asset('/Projects'),
            ],
            [
                'title' => 'Our Vision',
                'hash' => 'clients_testamonials',
                'url' => asset('/vision'),
            ],
            [
                'title' => 'Contact',
                'hash' => 'contact',
                'url' => asset('/contact'),
            ],
        ];

        $submenu_list = [
            [
                'title' => 'Branding',
                'url' => asset('/home/branding'),
            ],
            [
                'title' => 'Packaging',
                'url' => asset('/home/packaging'),
            ],
            [
                'title' => 'Design',
                'url' => asset('/home/design'),
            ],
            [
                'title' => 'Web / Mobile Dev',
                'url' => asset('/home/branding'),
            ],
        ];
    @endphp
</head>

<body>

    <!--== Loader Start ==-->
    <div id="loader-overlay">
        <div class="loader">
            <img src="assets/images/loader.svg" width="80" alt="">
        </div>
    </div>
    <!--== Loader End ==-->

    <!--== Wrapper Start ==-->
    <div class="wrapper">
        <!--== Header Start ==-->
        <nav class="navbar navbar-default navbar-fixed navbar-transparent white bootsnav on no-full no-border">
            <!--== Start Top Search ==-->
            <div class="fullscreen-search-overlay" id="search-overlay"> <a href="#" class="fullscreen-close"
                    id="fullscreen-close-button"><i class="icofont icofont-close"></i></a>
                <div id="fullscreen-search-wrapper">
                    <form method="get" id="fullscreen-searchform">
                        <input type="text" value="" placeholder="Type and hit Enter..." id="fullscreen-search-input"
                            class="search-bar-top">
                        <i class="fullscreen-search-icon icofont icofont-search">
                            <input value="" type="submit">
                        </i>
                    </form>
                </div>
            </div>
            <!--== End Top Search ==-->
            <div class="container-fluid">
                <!--== Start Atribute Navigation ==-->
                <div class="attr-nav hidden-xs sm-display-none">
                    <ul>
                        <li class="side-menu"><a href="#"><i class="icofont icofont-navigation-menu"></i></a></li>
                        <li class="search"><a href="#" id="search-button"><i
                                    class="icofont icofont-search"></i></a></li>
                    </ul>
                </div>
                <!--== End Atribute Navigation ==-->

                <!--== Start Header Navigation ==-->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <i
                            class="tr-icon ion-android-menu"></i> </button>
                    <div class="logo"> <a href="index.html"> <img class="logo logo-display"
                                src="{{ asset('assets/images/Karisma-Logo_white02.png') }}" alt=""> <img
                                class="logo logo-scrolled" src="{{ asset('assets/images/Karisma-Logo_dark02.png') }}"
                                alt="Logo"> </a> </div>
                </div>
                <!--== End Header Navigation ==-->

                <!--== Collect the nav links, forms, and other content for toggling ==-->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav navbar-center" data-in="fadeIn" data-out="fadeOut">
                        @foreach ($menu_list as $menu_item)
                            @if ($menu_item['title'] == 'Concepts')
                                <li class="dropdown"><a href="#{{ $menu_item['hash'] }}" class="dropdown-toggle"
                                        data-toggle="dropdown">{{ $menu_item['title'] }}</a>


                                    <ul class="dropdown-menu">
                                        @foreach ($submenu_list as $submenu)
                                            <li>
                                                <a href="{{ $submenu['url'] }}"
                                                    class="active">{{ $submenu['title'] }}</a>
                                            </li>
                                        @endforeach
                                        {{-- <li><a href="home-main.html">Main Demo</a></li>
                                        <li><a href="home-creative-agency.html">Home Creative Agency</a></li>
                                        <li><a href="home-digital.html">Home Digital</a></li>
                                        <li><a href="home-minimal-agency.html">Home Minimal Agency</a></li>
                                        <li><a href="home-business.html">Home Business</a></li>
                                        <li><a href="home-studio.html">Home Studio</a></li>
                                        <li><a href="home-startup.html">Home Startup</a></li>
                                        <li><a href="home-marketing.html">Home Marketing</a></li>
                                        <li><a href="home-parallax.html">Home Parallax</a></li>
                                        <li><a href="home-image-bg.html">Home Image Background</a></li>
                                        <li><a href="home-video-bg.html">Home Video Background</a></li>
                                        <li><a href="home-kenburn.html">Home Kenburn</a></li> --}}
                                    </ul>
                                @else
                                <li>
                                    <a class="page-scroll"
                                        href="#{{ $menu_item['hash'] }}">{{ $menu_item['title'] }}</a>
                                </li>
                            @endif


                            </li>
                        @endforeach
                        {{-- <li><a class="page-scroll" href="#feature">Features</a></li>
                        <li><a class="page-scroll" href="#portfolio">Portfolio</a></li>
                        <li><a class="page-scroll" href="#team">Team</a></li>
                        <li><a class="page-scroll" href="#pricing">Pricing</a></li>
                        <li><a class="page-scroll" href="#contact">Contact</a></li> --}}
                    </ul>
                </div>
                <!--== /.navbar-collapse ==-->
            </div>

            <!-- Start Side Menu -->
            <div class="side default-bg">
                <a href="index.html" class="logo-side"><img
                        src="{{ asset('assets/images/Karisma_logo_white_160x160.png') }}" alt="side-logo" /></a>
                <a href="#" class="close-side"><i class="icofont icofont-close"></i></a>
                <div class="widget mt-120">
                    <ul class="link">
                        @foreach ($menu_list as $menu_item)
                            <li class="link-item"><a class="page-scroll"
                                    href="#{{ $menu_item['hash'] }}">{{ $menu_item['title'] }}</a></li>
                        @endforeach
                        {{-- <li class="link-item"><a class="page-scroll" href="#feature">Services</a></li>
                        <li class="link-item"><a class="page-scroll" href="#portfolio">Projets</a></li>
                        <li class="link-item"><a class="page-scroll" href="#team">Team</a></li>
                        <li class="link-item"><a class="page-scroll" href="#pricing">Catalog</a></li>
                        <li class="link-item"><a class="page-scroll" href="#contact">Get in touch</a></li> --}}
                    </ul>
                </div>
                <ul class="social-media">
                    <li><a href="#" class="icofont icofont-social-facebook"></a></li>
                    <li><a href="#" class="icofont icofont-social-twitter"></a></li>
                    <li><a href="#" class="icofont icofont-social-behance"></a></li>
                    <li><a href="#" class="icofont icofont-social-dribble"></a></li>
                    <li><a href="#" class="icofont icofont-social-linkedin"></a></li>
                </ul>
            </div>
            <!-- End Side Menu -->

        </nav>
        <!--== Header End ==-->

        {{-- // Page Content --}}
        @yield('content')

        <!--== Footer Start ==-->
        <footer class="footer">
            <div class="footer-main">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <div class="widget widget-text">
                                <div class="logo-footer">
                                    <a href="index.html"><img class="img-responsive" src="assets/images/logo-footer.png"
                                            alt=""></a>
                                </div>
                                <p>Karisma provides fully flexible templates, allowing to customize every page.
                                </p>
                                <p class="mt-20 mb-0"><a href="#">+44 1234-567-8</a></p>
                                <p class="mt-0"><a
                                        href="https://www.google.com/maps/search/Potsdamer+Platz+9797/@52.5096488,13.3737554,17z/data=!3m1!4b1"
                                        target="_blank">Potsdamer Platz 9797</a></p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="widget widget-text widget-links">
                                <h5 class="widget-title">Join The Newsletter</h5>
                                <p>Lorem ipsum dolor sit amet, ut ius audiam denique tractatos.</p>
                                <form name="subscribe" id="subscribe" action="php/subscribe.php" method="POST"
                                    class="contact-form-style-02">
                                    <div class="form-group mt-20 mb-0">
                                        <label class="sr-only" for="name">Name</label>
                                        <input type="email" name="email" class="md-input style-02" id="email-2"
                                            placeholder="subscribe" required data-error="Your Name is Required">
                                    </div>
                                    <div class="text-left mt-10">
                                        <button type="submit" name="submit"
                                            class="btn btn-lg btn-color btn-square remove-margin">Subscribe</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="widget widget-links xs-mt-20 xs-mb-20 sm-mt-20 sm-mb-20">
                                <h5 class="widget-title">Quick Links</h5>
                                <ul>
                                    <li><a href="#">Engaging, purposeful, and creative.</a></li>
                                    <li><a href="#">Extroadinary life events.</a></li>
                                    <li><a href="#">Design your perfect event.</a></li>
                                    <li><a href="#">Connect your worlds.</a></li>
                                    <li><a href="#">Improving workplace productivity.</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="widget widget-links xs-mt-20 xs-mb-20 sm-mt-20 sm-mb-20">
                                <h5 class="widget-title">Media Gallery</h5>
                                <ul class="footer-gallery" id="footer-gallery">
                                    <li>
                                        <div class="footer-gallery-box">
                                            <div class="skin-overlay"></div>
                                            <img src="assets/images/gallery/01.jpg" alt="">
                                            <div class="zoom-wrap text-center">
                                                <ul class="footer-gallery-zoom">
                                                    <li><a class="magnific-lightbox"
                                                            href="assets/images/gallery/01-lg.jpg"><i
                                                                class="icofont icofont-search"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="footer-gallery-box">
                                            <div class="skin-overlay"></div>
                                            <img src="assets/images/gallery/02.jpg" alt="">
                                            <div class="zoom-wrap text-center">
                                                <ul class="footer-gallery-zoom">
                                                    <li><a class="magnific-lightbox"
                                                            href="assets/images/gallery/02-lg.jpg"><i
                                                                class="icofont icofont-search"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="footer-gallery-box">
                                            <div class="skin-overlay"></div>
                                            <img src="assets/images/gallery/03.jpg" alt="">
                                            <div class="zoom-wrap text-center">
                                                <ul class="footer-gallery-zoom">
                                                    <li><a class="magnific-lightbox"
                                                            href="assets/images/gallery/03-lg.jpg"><i
                                                                class="icofont icofont-search"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="footer-gallery-box">
                                            <div class="skin-overlay"></div>
                                            <img src="assets/images/gallery/04.jpg" alt="">
                                            <div class="zoom-wrap text-center">
                                                <ul class="footer-gallery-zoom">
                                                    <li><a class="magnific-lightbox"
                                                            href="assets/images/gallery/04-lg.jpg"><i
                                                                class="icofont icofont-search"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="footer-gallery-box">
                                            <div class="skin-overlay"></div>
                                            <img src="assets/images/gallery/05.jpg" alt="">
                                            <div class="zoom-wrap text-center">
                                                <ul class="footer-gallery-zoom">
                                                    <li><a class="magnific-lightbox"
                                                            href="assets/images/gallery/05-lg.jpg"><i
                                                                class="icofont icofont-search"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="footer-gallery-box">
                                            <div class="skin-overlay"></div>
                                            <img src="assets/images/gallery/06.jpg" alt="">
                                            <div class="zoom-wrap text-center">
                                                <ul class="footer-gallery-zoom">
                                                    <li><a class="magnific-lightbox"
                                                            href="assets/images/gallery/06-lg.jpg"><i
                                                                class="icofont icofont-search"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>

                                <div class="social-icons-style-03">
                                    <ul class="xs-icon mt-10">
                                        <li><a class="facebook" target="new"
                                                href="https://web.facebook.com/karismaconcepts/"><i
                                                    class="icofont icofont-social-facebook"></i></a></li>
                                        <li><a class="twitter" href="#."><i
                                                    class="icofont icofont-social-twitter"></i></a></li>
                                        <li><a class="behance" href="#."><i
                                                    class="icofont icofont-social-behance"></i></a></li>
                                        <li><a class="linkedin" href="#."><i
                                                    class="icofont icofont-social-linkedin"></i></a></li>
                                    </ul>
                                    <span class="white-color font-10px "> Tel: +237 65 015 51 00 / 695 48 69 09 </span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copy-right font-11px text-center">© 2019 - {{ Date('Y') }} Karisma
                                Concepts. All
                                rights
                                reserved
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <!--== Footer End ==-->

        <!--== Go to Top  ==-->
        <a href="javascript:" id="return-to-top"><i class="icofont icofont-arrow-up"></i></a>
        <!--== Go to Top End ==-->

    </div>
    <!--== Wrapper End ==-->

    <!--== Javascript Plugins ==-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJNGOwO2hJpJ9kz8e0UUPjZhEbgDJTTXE"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/master.js"></script>

    <!-- Revolution js Files -->
    <script src="js/jquery.themepunch.tools.min.js"></script>
    <script src="js/jquery.themepunch.revolution.min.js"></script>
    <script src="js/revolution.extension.actions.min.js"></script>
    <script src="js/revolution.extension.carousel.min.js"></script>
    <script src="js/revolution.extension.kenburn.min.js"></script>
    <script src="js/revolution.extension.layeranimation.min.js"></script>
    <script src="js/revolution.extension.migration.min.js"></script>
    <script src="js/revolution.extension.navigation.min.js"></script>
    <script src="js/revolution.extension.parallax.min.js"></script>
    <script src="js/revolution.extension.slideanims.min.js"></script>
    <script src="js/revolution.extension.video.min.js"></script>
    <!--== Javascript Plugins End ==-->

</body>

</html>
