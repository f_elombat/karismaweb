@extends('layouts.main')


@section('title', 'Home')

@section('content')
    <!--== Hero Slider Start ==-->

    <div class="remove-padding transition-none" id="home">
        <div id="rev_slider_347_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="parallaxjoy"
            data-source="gallery" style="background-color: #212121;padding:0px;">
            <!-- START REVOLUTION SLIDER 5.4.3.3 fullscreen mode -->
            <div id="rev_slider_347_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.3.3">
                <ul>
                    <!-- SLIDE  -->
                    <li data-index="rs-967" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                        data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="1000"
                        data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2=""
                        data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8=""
                        data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="assets/images/transparent.png"
                            data-bgcolor='linear-gradient(45deg, rgba(252, 238, 232,1)  0%, rgba(131, 114, 125,1) 100%)'
                            style='background:linear-gradient(45deg, rgba(252, 238, 232,1)  0%, rgba(131, 114, 125,1) 100%)'
                            alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"
                            data-bgparallax="off" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-resizeme" id="slide-967-layer-25" data-x="['left','left','left','left']"
                            data-hoffset="['21','21','21','-9']" data-y="['top','top','top','top']"
                            data-voffset="['45','45','45','30']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-type="image" data-responsive_offset="on"
                            data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:360deg;opacity:0;","to":"o:1;rZ:15;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 5;"><img src="assets/images/object/OBJ_triangle_s.png" alt=""
                                data-ww="['55px','55px','55px','56px']" data-hh="48px" width="111" height="96"
                                data-no-retina> </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption tp-resizeme rs-parallaxlevel-1" id="slide-967-layer-7"
                            data-x="['left','left','left','left']" data-hoffset="['-146','-146','-50','-86']"
                            data-y="['top','top','top','top']" data-voffset="['38','38','131','54']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on"
                            data-frames='[{"delay":200,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:240deg;opacity:0;fb:20px;","to":"o:1;rZ:-25;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 6;"><img src="assets/images/object/OBJ_pen.png" alt=""
                                data-ww="['530px','530px','300px','265px']" data-hh="['60px','60px','34px','30px']"
                                width="530" height="60" data-no-retina> </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-10"
                            id="slide-967-layer-20" data-x="['left','left','left','left']"
                            data-hoffset="['1079','1079','587','315']" data-y="['top','top','top','top']"
                            data-voffset="['512','512','692','471']" data-width="['100','100','100','50']"
                            data-height="['100','100','100','50']" data-whitespace="nowrap" data-type="shape"
                            data-responsive_offset="on"
                            data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:360deg;opacity:0;","to":"o:1;rZ:35;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 8;border-color:rgb(255,255,255);border-style:solid;border-width:3px 3px 3px 3px;">
                            <div class="rs-looped rs-wave" data-speed="8" data-angle="0" data-radius="3px"
                                data-origin="50% 50%"> </div>
                        </div>

                        <!-- LAYER NR. 8 -->
                        <div class="tp-caption tp-resizeme rs-parallaxlevel-2" id="slide-967-layer-2"
                            data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                            data-y="['middle','middle','middle','middle']" data-voffset="['-120','-120','-100','-80']"
                            data-fontsize="['100','100','80','50']" data-lineheight="['100','100','80','50']"
                            data-letterspacing="['30','30','30','20']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                            data-frames='[{"delay":550,"split":"chars","splitdelay":0.1000000000000000055511151231257827021181583404541015625,"speed":2500,"split_direction":"random","frame":"0","from":"y:50px;sX:1;sY:1;opacity:0;fb:20px;","color":"#001536","to":"o:1;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[30,30,30,20]"
                            style="z-index: 13; white-space: nowrap; font-size: 100px; line-height: 100px; font-weight: 800; color: #ffffff;font-family: 'Montserrat', sans-serif;">
                            @yield('tagline1', "KARISMA") </div>

                        <!-- LAYER NR. 9 -->
                        <div class="tp-caption tp-resizeme rs-parallaxlevel-2" id="slide-967-layer-3"
                            data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                            data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                            data-fontsize="['100','100','80','50']" data-lineheight="['100','100','80','50']"
                            data-letterspacing="['30','30','30','20']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                            data-frames='[{"delay":600,"split":"chars","splitdelay":0.1000000000000000055511151231257827021181583404541015625,"speed":2500,"split_direction":"random","frame":"0","from":"y:50px;sX:1;sY:1;opacity:0;fb:20px;","color":"#001536","to":"o:1;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[30,30,30,20]"
                            style="z-index: 14; white-space: nowrap; font-size: 100px; line-height: 100px; font-weight: 800; color: #ffffff;font-family: 'Montserrat', sans-serif;">
                            @yield('tagline2', "CONCEPTS") </div>

                        <!-- LAYER NR. 10 -->
                        <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-10"
                            id="slide-967-layer-27" data-x="['left','left','left','left']"
                            data-hoffset="['1047','1047','715','715']" data-y="['top','top','top','top']"
                            data-voffset="['728','728','574','574']" data-width="50" data-height="100"
                            data-whitespace="nowrap" data-type="shape" data-responsive_offset="on"
                            data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:360deg;opacity:0;","to":"o:1;rZ:15;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 16;border-color:rgb(255,255,255);border-style:solid;border-width:3px 3px 3px 3px;border-radius:25px 25px 25px 25px;">
                            <div class="rs-looped rs-wave" data-speed="5" data-angle="0" data-radius="4px"
                                data-origin="50% 50%"> </div>
                        </div>

                        <!-- LAYER NR. 11 -->
                        <div class="tp-caption tp-resizeme rs-parallaxlevel-4" id="slide-967-layer-12"
                            data-x="['left','left','left','left']" data-hoffset="['687','687','452','271']"
                            data-y="['top','top','top','top']" data-voffset="['356','356','499','328']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on"
                            data-frames='[{"delay":200,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:-140deg;opacity:0;fb:20px;","to":"o:1;rZ:-15;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 17;"><img src="assets/images/object/OBJ_ipad.png" alt=""
                                data-ww="['710px','710px','400px','355px']" data-hh="['630px','630px','355px','315px']"
                                width="710" height="630" data-no-retina>
                        </div>



                        <!-- LAYER NR. 13 -->
                        <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-10"
                            id="slide-967-layer-21" data-x="['left','left','left','left']"
                            data-hoffset="['1158','1158','658','416']" data-y="['top','top','top','top']"
                            data-voffset="['19','19','334','60']" data-width="['100','100','50','50']"
                            data-height="['100','100','50','50']" data-whitespace="nowrap" data-type="shape"
                            data-responsive_offset="on"
                            data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 19;border-color:rgb(255,255,255);border-style:solid;border-width:3px 3px 3px 3px;border-radius:50px 50px 50px 50px;">
                            <div class="rs-looped rs-wave" data-speed="3" data-angle="0" data-radius="4px"
                                data-origin="50% 50%"> </div>
                        </div>


                        <!-- LAYER NR. 15 -->
                        <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-10"
                            id="slide-967-layer-22" data-x="['left','left','left','left']"
                            data-hoffset="['-203','-203','-4','42']" data-y="['top','top','top','top']"
                            data-voffset="['349','349','328','360']" data-width="['100','100','100','50']"
                            data-height="['100','100','100','50']" data-whitespace="nowrap" data-type="shape"
                            data-responsive_offset="on"
                            data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 21;border-color:rgb(255,255,255);border-style:solid;border-width:3px 3px 3px 3px;border-radius:50px 50px 50px 50px;">
                            <div class="rs-looped rs-wave" data-speed="6" data-angle="0" data-radius="6px"
                                data-origin="50% 50%"> </div>
                        </div>

                        <!-- LAYER NR. 16 -->
                        <div class="tp-caption tp-resizeme rs-parallaxlevel-3" id="slide-967-layer-10"
                            data-x="['left','left','left','left']" data-hoffset="['-294','-294','-82','-106']"
                            data-y="['top','top','top','top']" data-voffset="['179','179','285','158']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on"
                            data-frames='[{"delay":200,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:180deg;sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;rZ:15;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 22;"><img src="assets/images/object/OBJ_phone.png" alt=""
                                data-ww="['390px','390px','250px','195px']" data-hh="['620px','620px','397px','310px']"
                                width="390" height="620" data-no-retina>
                        </div>

                        <!-- LAYER NR. 17 -->
                        <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-967-layer-16"
                            data-x="['left','left','center','left']" data-hoffset="['100','100','0','-226']"
                            data-y="['bottom','bottom','bottom','bottom']" data-voffset="['50','50','30','64']"
                            data-width="200" data-height="5" data-whitespace="nowrap"
                            data-visibility="['on','on','on','off']" data-type="shape" data-basealign="slide"
                            data-responsive_offset="on"
                            data-frames='[{"delay":2500,"speed":1000,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:[-100%];y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 24;background-color:rgb(255,255,255);">
                        </div>

                        <!-- LAYER NR. 18 -->
                        <div class="tp-caption tp-resizeme tp-svg-layer" id="slide-967-layer-17"
                            data-x="['left','left','center','left']" data-hoffset="['175','175','0','-153']"
                            data-y="['bottom','bottom','bottom','bottom']" data-voffset="['86','86','50','91']"
                            data-width="50" data-height="50" data-whitespace="nowrap"
                            data-visibility="['on','on','on','off']" data-type="svg"
                            data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":"","speed":"1500","ease":"Power4.easeOut"}]'
                            data-svg_src="assets/images/object/ic_arrow_downward_24px.svg"
                            data-svg_idle="sc:transparent;sw:0;sda:0;sdo:0;" data-basealign="slide"
                            data-responsive_offset="on"
                            data-frames='[{"delay":2500,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 25; min-width: 50px; max-width: 50px; max-width: 50px; max-width: 50px; color: #ffffff;cursor:pointer;">
                            <div class="rs-looped rs-slideloop" data-easing="Power1.easeIn" data-speed="1" data-xs="0"
                                data-xe="0" data-ys="-10" data-ye="10"> </div>
                        </div>

                        <!-- LAYER NR. 19 -->
                        <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-12"
                            id="slide-967-layer-19" data-x="['left','left','left','left']"
                            data-hoffset="['94','94','123','39']" data-y="['top','top','top','top']"
                            data-voffset="['77','77','138','65']" data-width="['60','60','60','30']"
                            data-height="['60','60','60','30']" data-whitespace="nowrap" data-type="shape"
                            data-responsive_offset="on"
                            data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 26;border-color:rgb(255,255,255);border-style:solid;border-width:3px 3px 3px 3px;border-radius:50px 50px 50px 50px;">
                            <div class="rs-looped rs-wave" data-speed="6" data-angle="0" data-radius="5px"
                                data-origin="50% 50%"> </div>
                        </div>

                        <!-- LAYER NR. 20 -->
                        <div class="tp-caption tp-resizeme rs-parallaxlevel-12" id="slide-967-layer-23"
                            data-x="['left','left','left','left']" data-hoffset="['1081','1081','657','394']"
                            data-y="['top','top','top','top']" data-voffset="['109','109','128','298']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on"
                            data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:360deg;opacity:0;","to":"o:1;rZ:25;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 27;">
                            <div class="rs-looped rs-wave" data-speed="7" data-angle="0" data-radius="5px"
                                data-origin="50% 50%"><img src="assets/images/object/OBJ_triangle.png" alt=""
                                    data-ww="['110px','110px','110px','110px']" data-hh="['95px','95px','95px','95px']"
                                    width="221" height="191" data-no-retina>
                            </div>
                        </div>

                        <!-- LAYER NR. 21 -->
                        <div class="tp-caption tp-resizeme rs-parallaxlevel-12" id="slide-967-layer-24"
                            data-x="['left','left','left','left']" data-hoffset="['-31','-31','31','-34']"
                            data-y="['top','top','top','top']" data-voffset="['548','548','589','494']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on"
                            data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:360deg;opacity:0;","to":"o:1;rZ:-40;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 28;">
                            <div class="rs-looped rs-wave" data-speed="6" data-angle="0" data-radius="6px"
                                data-origin="50% 50%"><img src="assets/images/object/OBJ_triangle.png" alt=""
                                    data-ww="['110px','110px','110px','110px']" data-hh="['95px','95px','95px','95px']"
                                    width="221" height="191" data-no-retina>
                            </div>
                        </div>

                        <!-- LAYER NR. 22 -->
                        <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-12"
                            id="slide-967-layer-26" data-x="['left','left','left','left']"
                            data-hoffset="['42','42','24','57']" data-y="['top','top','top','top']"
                            data-voffset="['257','257','209','226']" data-width="['40','40','40','20']"
                            data-height="['40','40','40','20']" data-whitespace="nowrap" data-type="shape"
                            data-responsive_offset="on"
                            data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:360deg;opacity:0;","to":"o:1;rZ:45;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 29;border-color:rgb(255,255,255);border-style:solid;border-width:3px 3px 3px 3px;">
                            <div class="rs-looped rs-wave" data-speed="5" data-angle="0" data-radius="5px"
                                data-origin="50% 50%"> </div>
                        </div>

                        <!-- LAYER NR. 23 -->
                        <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-11"
                            id="slide-967-layer-28" data-x="['left','left','left','left']"
                            data-hoffset="['1173','1173','764','764']" data-y="['top','top','top','top']"
                            data-voffset="['652','652','723','723']" data-width="30" data-height="30"
                            data-whitespace="nowrap" data-type="shape" data-responsive_offset="on"
                            data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:360deg;opacity:0;","to":"o:1;rZ:-15;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                            style="z-index: 30;border-color:rgb(255,255,255);border-style:solid;border-width:3px 3px 3px 3px;">
                            <div class="rs-looped rs-wave" data-speed="8" data-angle="0" data-radius="8px"
                                data-origin="50% 50%"> </div>
                        </div>
                    </li>
                </ul>
                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
            </div>
        </div>
    </div>
    <!--== Hero Slider End ==-->

    <!--== What We Offer Start ==-->
    <section id="feature" class="white-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 xs-mb-50">
                    <div class="section-title text-left">
                        <h1 data-backfont-sm="Design">Karisma Concepts, a professional web <br>and graphic <br> <span
                                class="text-bottom-line-sm">design agency</span></h1>
                        <p class="mt-50 font-26px line-height-40">Karisma provides in digital marketing services, video
                            content creation and website development, e-commerce, SEO and strategy.
                        </p>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <img src="assets/images/video-mockup.png" alt="video-1" class="img-responsive" />
                    <div class="video-box_overlay">
                        <div class="center-layout">
                            <div class="v-align-middle"> <a class="popup-youtube"
                                    href="https://www.youtube.com/watch?v=sU3FkzUKHXU">
                                    <div class="play-button"><i class="tr-icon ion-android-arrow-dropright"></i>
                                    </div>
                                </a> </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-50 tabs-style-01">
                <div class="col-md-12">
                    <div class="icon-tabs">
                        <!--== Nav tabs ==-->
                        <ul class="nav nav-tabs text-center" role="tablist">
                            <li role="presentation" class="active"><a href="#GraphicDesign" role="tab"
                                    data-toggle="tab">
                                    <h1>01</h1> Graphic Design
                                </a></li>
                            <li role="presentation"><a href="#PackagingDesign" role="tab" data-toggle="tab">
                                    <h1>02</h1> Packaging Design
                                </a></li>
                            <li role="presentation"><a href="#DigitalMarketing" role="tab" data-toggle="tab">
                                    <h1>03</h1> Digital Marketing
                                </a></li>
                            <li role="presentation"><a href="#apps" role="tab" data-toggle="tab">
                                    <h1>04</h1> Web/Mobile Apps
                                </a></li>
                        </ul>
                        <!--== Tab panes ==-->
                        <div class="tab-content text-center">
                            <div role="tabpanel" class="tab-pane fade in active" id="GraphicDesign">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="text-left">
                                            <h3 class="font-700">Graphic Design</h3>
                                            <p>Graphic Design is the process of creating websites. It encompasses
                                                several different aspects, including webpage layout, content
                                                production, and graphic design.</p>
                                            <p>While the terms Graphic Design and web development are often used
                                                interchangeably, Graphic Design is technically a subset of the broader
                                                category of web development.</p>
                                            <p class="mt-50"><a
                                                    class="btn btn-md btn-color btn-animate btn-circle"><span>Buy
                                                        Template <i
                                                            class="tr-icon icofont icofont-arrow-right"></i></span></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <img class="img-responsive" src="assets/images/iMac-screen.png" alt="tab-1" />
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="PackagingDesign">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="text-left">
                                            <h3 class="font-700">Packaging Design</h3>
                                            <p>Packaging Design refers to building, creating, and an maintaining
                                                websites. It includes aspects such as web design, web publishing,
                                                web programming, and database management.</p>
                                            <p>While the terms "web developer" and "web designer" are often used
                                                synonymously, they do not mean the same thing.</p>
                                            <p class="mt-50"><a
                                                    class="btn btn-md btn-color btn-animate btn-circle"><span>Buy
                                                        Template <i
                                                            class="tr-icon icofont icofont-arrow-right"></i></span></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <img class="img-responsive" src="assets/images/macbook-screen.png" alt="tab-2" />
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="DigitalMarketing">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="text-left">
                                            <h3 class="font-700">Digital Marketing</h3>
                                            {{-- <p>Search engine optimization is the process of increasing the quality
                                                and quantity of website traffic by increasing the visibility of a
                                                website or a web page to users of a web search engine.</p>
                                            <p>SEO refers to the improvement of unpaid results, and excludes direct
                                                traffic/visitors and the purchase of paid placement.</p> --}}
                                            <p>Digital Marketing is ....
                                                Lorem, ipsum dolor sit amet consectetur adipisicing
                                                elit.
                                                Aspernatur aliquid quam alias repellat. Quam est molestiae quaerat. Rem,
                                                eveniet asperiores
                                                temporibus quas minus sit dicta repellendus odio ipsum nemo fugiat.Itaque
                                                at, omnis accusantium
                                                libero iure quibusdam soluta nostrum voluptatem sapiente! Doloremque aliquid
                                                autem at, consequuntur
                                                ducimus aperiam numquam vitae nostrum eos accusamus molestiae nemo
                                                repudiandae porro nisi magni.</p>
                                            <p class="mt-50"><a
                                                    class="btn btn-md btn-color btn-animate btn-circle"><span>Buy
                                                        Template <i
                                                            class="tr-icon icofont icofont-arrow-right"></i></span></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <img class="img-responsive" src="assets/images/ipad-screen.png" alt="tab-3" />
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="apps">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="text-left">
                                            <h3 class="font-700">Web/Mobile Apps</h3>
                                            <p>A mobile application, most commonly referred to as an app, is a type
                                                of application software designed to run on a mobile device, such as
                                                a smartphone or tablet computer. </p>
                                            <p>Apps are generally downloaded from application distribution platforms
                                                which are operated by the owner of the mobile operating system, such
                                                as the App Store (iOS) or Google Play Store.</p>
                                            <p class="mt-50"><a
                                                    class="btn btn-md btn-color btn-animate btn-circle"><span>Buy
                                                        Template <i
                                                            class="tr-icon icofont icofont-arrow-right"></i></span></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <img class="img-responsive" src="assets/images/browser-screen.png" alt="tab-4" />
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--== What We Offer End ==-->

    <!--== Portfolio Start ==-->
    <section id="portfolio" class="pt-0 white-bg pb-0">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="section-title text-left text-sm-center text-xs-center">
                        <h1 data-backfont-sm="Portfolio">Take a Look at Our Featured <span
                                class="text-bottom-line-sm">Work</span></h1>
                    </div>
                </div>
            </div>
            <div class="row mt-80">
                <div class="col-md-12">
                    <div id="portfolio-gallery" class="cbp">
                        <div class="cbp-item branding col-md-3 col-sm-6 creative">
                            <div class="portfolio-item">
                                <a href="#">
                                    <img src="assets/images/portfolio/grid/1.jpg" alt="">
                                    <div class="portfolio-info dark-bg">
                                        <div class="centrize">
                                            <div class="v-center white-color">
                                                <h3>Creative Design</h3>
                                                <p>Branding</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="cbp-item print branding col-md-3 col-sm-6">
                            <div class="portfolio-item">
                                <a href="#">
                                    <img src="assets/images/portfolio/grid/2.jpg" alt="">
                                    <div class="portfolio-info dark-bg">
                                        <div class="centrize">
                                            <div class="v-center white-color">
                                                <h3>Cup Design</h3>
                                                <p>Branding</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="cbp-item stationery branding web-design col-md-6 col-sm-6">
                            <div class="portfolio-item">
                                <a href="#">
                                    <img src="assets/images/portfolio/grid/3.jpg" alt="">
                                    <div class="portfolio-info dark-bg">
                                        <div class="centrize">
                                            <div class="v-center white-color">
                                                <h3>Shoptify</h3>
                                                <p>Branding</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="cbp-item branding creative col-md-6 col-sm-6">
                            <div class="portfolio-item">
                                <a href="#">
                                    <img src="assets/images/portfolio/grid/4.jpg" alt="">
                                    <div class="portfolio-info dark-bg">
                                        <div class="centrize">
                                            <div class="v-center white-color">
                                                <h3>Game of Neurones</h3>
                                                <p>Branding</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="cbp-item print photography col-md-3 col-sm-6">
                            <div class="portfolio-item">
                                <a href="#">
                                    <img src="assets/images/portfolio/grid/5.jpg" alt="">
                                    <div class="portfolio-info dark-bg">
                                        <div class="centrize">
                                            <div class="v-center white-color">
                                                <h3>Krausz</h3>
                                                <p>Branding</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="cbp-item branding creative col-md-3 col-sm-6">
                            <div class="portfolio-item">
                                <a href="#">
                                    <img src="assets/images/portfolio/grid/6.jpg" alt="">
                                    <div class="portfolio-info dark-bg">
                                        <div class="centrize">
                                            <div class="v-center white-color">
                                                <h3>Blend Station</h3>
                                                <p>Branding</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--== Portfolio End ==-->

    <!--== Process Start ==-->
    <section style="background:url(assets/images/background/stars-bg-4.png) center center no-repeat #fff;"
        class="xs-pt-80 xs-pb-80">
        <div class="container">
            <div class="row our-process-style-01">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 line mb-30">
                    <div class="text-left">
                        <i class="dark-color icon-puzzle font-50px"></i>
                        <h2 class="mt-20 default-color font-700">Design</h2>
                        <p class="mb-0 mt-20 dark-color">Web design is the process of creating websites. It
                            encompasses several different aspects, including webpage layout. </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 line mb-30">
                    <div class="text-left">
                        <i class="dark-color icon-globe font-50px"></i>
                        <h2 class="mt-20 default-color font-700">Development</h2>
                        <p class="mb-0 mt-20 dark-color">Web development refers to building, creating, and an
                            maintaining websites. It includes aspects such as web design. </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 line mb-30">
                    <div class="text-left">
                        <i class="dark-color icon-gears font-50px"></i>
                        <h2 class="mt-20 default-color font-700">Testing</h2>
                        <p class="mb-0 mt-20 dark-color">Test design is a process that describes “how” testing
                            should be done. It includes processes for the identifying test cases.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 line mb-30">
                    <div class="text-left">
                        <i class="dark-color icon-browser font-50px"></i>
                        <h2 class="mt-20 default-color font-700">Live</h2>
                        <p class="mb-0 mt-20 dark-color">Web testing is a software testing practice to test
                            websites or web applications for potential bugs. It's a complete testing.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--== Process End ==-->

    <!--== Testimonails Start ==-->
    <section id="clients_testamonials">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="section-title text-left text-sm-center text-xs-center">
                        <h1 data-backfont-sm="Clients">Our<br> Happy <span class="text-bottom-line-sm">Customers</span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row mt-30">
                <div class="testimonial">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <!--== Slide ==-->
                        <div class="testimonial-item">
                            <div class="testimonial-content border-radius-5 box-shadow">
                                <p class="line-height-26 font-14px"><i
                                        class="icofont icofont-quote-left font-50px default-color mt-20"></i>
                                    <span>Very much satisfied with the service. Delivered on time and responded to
                                        request for modifications within few hours. I recommend Sloganshub for
                                        anyone looking for smart business.</span>
                                </p>
                                <img class="img-responsive" alt="avatar-1" src="assets/images/team/avatar-1.jpg" />
                                <h5 class="font-700 mb-0">Stéphane K.</h5>
                                <h5 class="font-500 default-color mt-10">Eneo S.A</h5>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <!--== Slide ==-->
                        <div class="testimonial-item text-center">
                            <div class="testimonial-content border-radius-5 box-shadow">
                                <p class="line-height-26 font-14px"><i
                                        class="icofont icofont-quote-left font-50px default-color mt-20"></i>
                                    <span>I’m very happy with the results! They went above and beyond for me and
                                        that is always appreciated! I would highly recommend SH to anybody!</span>
                                </p>
                                <img class="img-responsive" alt="avatar-2" src="assets/images/team/avatar-2.jpg" />
                                <h5 class="font-700 mb-0">Michael Kyle</h5>
                                <h5 class="font-500 default-color mt-10">Les Marches d'élodie</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <!--== Slide ==-->
                        <div class="testimonial-item text-center">
                            <div class="testimonial-content border-radius-5 box-shadow">
                                <p class="line-height-26 font-14px"><i
                                        class="icofont icofont-quote-left font-50px default-color mt-20"></i>
                                    <span>Great work I got a lot more than what I ordered, they are very legítimas
                                        and catchy. I went for one of them for my brand but is always better to have
                                        more options.</span>
                                </p>
                                <img class="img-responsive" alt="avatar-1" src="assets/images/team/avatar-3.jpg" />
                                <h5 class="font-700 mb-0">Mellissa Christine</h5>
                                <h5 class="font-500 default-color mt-10">Camtel S.A</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <!--== Slide ==-->
                        <div class="testimonial-item text-center">
                            <div class="testimonial-content border-radius-5 box-shadow">
                                <p class="line-height-26 font-14px"><i
                                        class="icofont icofont-quote-left font-50px default-color mt-20"></i>
                                    <span>Great work I got a lot more than what I ordered, they are very legítimas
                                        and catchy. I went for one of them for my brand but is always better to have
                                        more options.</span>
                                </p>
                                <img class="img-responsive" alt="avatar-1" src="assets/images/team/avatar-4.jpg" />
                                <h5 class="font-700 mb-0">Jane Torres</h5>
                                <h5 class="font-500 default-color mt-10">StartUp Academie</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--== Testimonails End ==-->

    <!--== Team Start ==-->
    <section style="background:url(assets/images/background/stars-bg.png) center center no-repeat #212121;" id="team">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="section-title text-left text-sm-center text-xs-center">
                        <h1 data-backfont-sm="Crew" class="white-color">Our Warm Up <br> Is Your <span
                                class="text-bottom-line-sm">Work Out</span></h1>
                    </div>
                </div>
            </div>

            <div class="row mt-50">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="team-member">
                        <div class="team-thumb">
                            <div class="thumb-overlay"></div>
                            <img src="assets/images/team/team-1.jpg" class="border-radius-10" alt="">
                            <div class="member-info text-center white-bg">
                                <h3>Tom Bills</h3>
                                <span class="title">Web Designer</span>
                                <div class="social-icons-style-09">
                                    <ul class="sm-icon mt-20">
                                        <li><a class="facebook" href="#."><i
                                                    class="icofont icofont-social-facebook"></i></a></li>
                                        <li><a class="twitter" href="#."><i
                                                    class="icofont icofont-social-twitter"></i></a></li>
                                        <li><a class="behance" href="#."><i
                                                    class="icofont icofont-social-behance"></i></a></li>
                                        <li><a class="linkedin" href="#."><i
                                                    class="icofont icofont-social-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--== Member End ==-->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="team-member">
                        <div class="team-thumb">
                            <div class="thumb-overlay"></div>
                            <img src="assets/images/team/team-2.jpg" class="border-radius-10" alt="">
                            <div class="member-info text-center white-bg">
                                <h3>Sara Adams</h3>
                                <span class="title">CEO / Founder</span>
                                <div class="social-icons-style-09">
                                    <ul class="sm-icon mt-20">
                                        <li><a class="facebook" href="#."><i
                                                    class="icofont icofont-social-facebook"></i></a></li>
                                        <li><a class="twitter" href="#."><i
                                                    class="icofont icofont-social-twitter"></i></a></li>
                                        <li><a class="behance" href="#."><i
                                                    class="icofont icofont-social-behance"></i></a></li>
                                        <li><a class="linkedin" href="#."><i
                                                    class="icofont icofont-social-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--== Member End ==-->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="team-member">
                        <div class="team-thumb">
                            <div class="thumb-overlay"></div>
                            <img src="assets/images/team/team-3.jpg" class="border-radius-10" alt="">
                            <div class="member-info text-center white-bg">
                                <h3>Enzo William</h3>
                                <span class="title">Photographer</span>
                                <div class="social-icons-style-09">
                                    <ul class="sm-icon mt-20">
                                        <li><a class="facebook" href="#."><i
                                                    class="icofont icofont-social-facebook"></i></a></li>
                                        <li><a class="twitter" href="#."><i
                                                    class="icofont icofont-social-twitter"></i></a></li>
                                        <li><a class="behance" href="#."><i
                                                    class="icofont icofont-social-behance"></i></a></li>
                                        <li><a class="linkedin" href="#."><i
                                                    class="icofont icofont-social-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!--== Team End ==-->

    <!--== Video Start ==-->
    <section class="parallax-bg fixed-bg xs-pt-40 xs-pb-40"
        data-parallax-bg-image="assets/images/background/parallax-bg-2.jpg" data-parallax-speed="0.5"
        data-parallax-direction="up">
        <div class="overlay-bg"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 text-center parallax-content height-400px centerize-col">
                    <div class="center-layout">
                        <div class="v-align-middle">
                            <h1 class="font-800 white-color xs-font-40px">Let Your Email Address Speak for You</h1>
                            <h3 class="white-color">Join us on social media</h3>
                            <p class="mt-30"><a class="btn btn-lg btn-dark btn-square">Read more</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--== Video End ==-->

    <!--== Pricing Table Start ==-->
    <section id="pricing">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="section-title text-left text-sm-center text-xs-center">
                        <h1 data-backfont-sm="Pricing">Panoramic Solutions<br> Top <span class="text-bottom-line-sm">To
                                Bottom</span></h1>
                    </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-md-4 pricing-table col-sm-4">
                    <div class="pricing-box border-radius-5">
                        <h3 class="dark-color mb-0">Personal </h3>
                        <h4 class="dark-color">Basic and simple website. - One Page </h4>
                        <p class="font-10px">Perfect to start his activity or a first visibility on the web.</p>

                        <h2 class="font-50px dark-color sm-font-20px">
                            <sup class="font-14px">fr CFA</sup>
                            <span>35<sup>000</sup> </span>
                        </h2>
                        <ul>
                            {{-- <li>Mobile-Optimized Website</li>
                            <li>Powerful Website Metrics</li>
                            <li>Free Custom Domain</li>
                            <li>24/7 Customer Support</li>
                            <li>Fully Integrated E-Commerce</li>
                            <li>Sell Unlimited Products &amp; Accept Donations</li> --}}
                            {!! "
                                <li>Server and SSL settings,</li>
                                <li>The development of a unique page,</li>

                                <li>Available features :</li>
                                <li>&middot; Site in 1 language,</li>
                                <li>&middot; Adaptable to mobiles,</li>

                                <li>Access to administration of the site.</li>
                                <li>1 Free Custom Domain</li>
                                <li>SEO package :</li>
                                <li>&middot; Local referencing,</li>
                                <li>&middot; Addition of elements for SEO,</li>
                                <li>&middot; Google Analytics </li>
                                <li>24/7 Customer Support</li> " !!}
                        </ul>
                        <div class="pricing-box-bottom"> <a class="btn btn-dark btn-lg btn-square full-width">Try
                                it Now</a> </div>
                    </div>
                </div>

                <div class="col-md-4 pricing-table col-sm-4 featured">
                    <div class="pricing-box featured border-radius-5">
                        <div class="featured-mark">
                            Featured
                        </div>
                        <h3 class="default-color mb-0">Professional</h3>
                        <h4 class="dark-color">Proffesional Kit and simple website.</h4>
                        <p class="font-10px">Perfect to start his activity or a first visibility on the web.</p>
                        <h2 class="font-50px default-color sm-font-20px">
                            <sup class="font-14px">fr CFA</sup>
                            <span>65<sup>000</sup> </span>
                        </h2>


                        <div class="pricicng-feature">
                            <ul>
                                <li>The development of 4 to 5 pages (home, presentation,services, references, contact)</li>
                                <li>including the TPE Services</li>
                                <li>Fully Integrated E-Commerce</li>
                                <li>Sell Unlimited Products &amp; Accept Donations</li>
                            </ul>
                        </div>
                        <div class="pricing-box-bottom"> <a class="btn btn-color btn-lg btn-square full-width">Try
                                it Now</a> </div>
                    </div>
                </div>

                <div class="col-md-4 pricing-table col-sm-4">
                    <div class="pricing-box border-radius-5">
                        <h3 class="dark-color mb-0">Ultimate</h3>

                        <h4 class="dark-color">Business kit and tailor made website.</h4>
                        <p class="font-10px">Ideal for SMEs to boost its activity on the web with a better
                            visibility.</p>
                        <h2 class="font-50px dark-color sm-font-20px">
                            <sup class="font-14px">fr CFA</sup>
                            <span>125<sup>000</sup> </span>
                        </h2>
                        <ul>
                            {{-- <li>including the Pro Services</li>
                            <li>Free Custom SubDomain</li>
                            <li>24/7 Customer Support</li>
                            <li>Fully Integrated E-Commerce</li>
                            <li>Sell Unlimited Products &amp; Accept Donations</li> --}}

                            <li> Development up to 10 pages,</li>
                            <li>Custom design,</li>
                            <li>Enhanced security.</li>
                            <li>Available features:</li>
                            <li>&middot; Site up to 2 languages,</li>
                            <li>&middot; News space / blog,</li>

                            <li>&middot; Site administration access.</li>
                            <li>Modules specific to your activity.</li>
                        </ul>
                        <div class="pricing-box-bottom"> <a class="btn btn-dark btn-lg btn-square full-width">Try
                                it Now</a> </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--== Pricing Table End ==-->

    <!--== Contact Start ==-->
    <section class="transition-none" id="contact"
        style="background:url(assets/images/background/stars-bg-4.png) center center no-repeat #fff;">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="client-logo"> <img class="img-responsive" src="assets/images/clients/logo-1-dark.png"
                            alt="01" /> </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="client-logo"> <img class="img-responsive" src="assets/images/clients/logo-2-dark.png"
                            alt="02" /> </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="client-logo"> <img class="img-responsive" src="assets/images/clients/logo-3-dark.png"
                            alt="03" /> </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="client-logo"> <img class="img-responsive" src="assets/images/clients/logo-4-dark.png"
                            alt="04" /> </div>
                </div>
            </div>
            <div class="row mt-100">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h1 class="dark-color">Request a Quote <span class="text-bottom-line-sm">Now</span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-sm-10 centerize-col">
                    <form name="contact-form" id="contact-form" action="php/contact.php" method="POST"
                        class="contact-form-style-03 dark-bg all-padding-40 mt-50 border-radius-5">
                        <div class="messages"></div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="md-input style-02" id="name" placeholder="Name *"
                                        required data-error="Your Name is Required">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" class="md-input style-02" id="email"
                                        placeholder="Email *" required data-error="Please Enter Valid Email">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="subject">Subject</label>
                                    <input type="email" name="email" class="md-input style-02" id="subject"
                                        placeholder="Subject">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label for="subject">Your Budget</label>
                                <select name="orderby" class="orderby">
                                    <option value="" selected="selected">$500 - $1000</option>
                                    <option value="">$1000 - $2000</option>
                                    <option value="">$2000 - $5000</option>
                                </select>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="message">Project Details</label>
                                    <textarea name="message" class="md-textarea style-02" id="message" rows="7"
                                        placeholder="Project Details" required
                                        data-error="Please, Leave us a message"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="text-center">
                                    <button type="submit" name="submit"
                                        class="btn btn-lg btn-color btn-circle remove-margin">Send Message
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>
    <!--== Contact End ==-->
@endsection
