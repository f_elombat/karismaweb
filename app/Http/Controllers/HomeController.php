<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Prep data pointer
        $data = [];


        // $data['submenu_list'] = $submenu_list;
        return  view('home.index', compact($data));
    }
}
